# My project's README

Orig. instruction.
http://vps2.etotheipiplusone.com:30176/redmine/projects/emscripten-qt/wiki

1. git clone -b ship git@bitbucket.org:aksenofo/emscripten-qt.git

2. mkdir build-emscripten-qt

3. set up env.
   export EMSCRIPTEN_ROOT_PATH=PATH-TO-REAL em++

4. run script
../emscripten-qt/configure -xplatform qws/emscripten-clang \
-embedded emscripten -static -opensource -debug  -no-qt3support -no-opengl -no-openssl \
-system-zlib -no-gif -qt-zlib -qt-libpng -no-libmng -no-libtiff -qt-libjpeg -no-accessibility \
-dbus -script -no-fpu -no-mmx -no-3dnow -no-sse -no-sse2 -no-sse3 -no-ssse3 -no-sse4.1 -no-sse4.2 -no-icu -no-rpath \
-confirm-license -no-webkit -no-phonon -no-freetype -nomake demos -nomake examples -little-endian -no-feature-socket \
-no-feature-codecs -no-feature-textcodecplugin -no-feature-systemlocale  -no-feature-qws_multiprocess -no-feature-sound \
-no-feature-printpreviewwidget  -no-feature-printpreviewdialog  -no-feature-systemsemaphore -no-feature-sharedmemory \
-no-feature-localeventloop -feature-qws_clientblit -feature-qws_cursor  -depths 32 -make tools \
-no-javascript-jit -no-script -no-scripttools \
--prefix=$(pwd)/../emscripten-qt-install

5. say: make

6. say make install

---------------------------------------------------------------------------------------------------------
Known error case
/bin/sh: 1: EMSCRIPTEN_ROOT_PATH: not found
/bin/sh: 1: /em++: not found
make: *** [project.o] Error 127
Normally it means that env broken. Open new console.
